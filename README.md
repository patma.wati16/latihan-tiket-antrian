# Pengenalan Konsep MVC Golang
This section facilitate formation competencies to be able to understand basic concepts Golang and the concept of using the MVC implementation.
Golang.
## Development Tutorial

### 1. Install Gin Package
To see the complete documentation of Gin, you can visit the Gin official [repository][gin-repository].
```
go get github.com/gin-gonic/gin
```
### 2. Install Firebase Admin SDK 
```
go get firebase.google.com/go
```

[gin-repository]: <https://github.com/gin-gonic/gin>
