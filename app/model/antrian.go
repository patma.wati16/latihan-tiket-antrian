package model

import (
	"firebase.google.com/go/db"
	"fmt"
	"log"
	"strings"
	"time"
)

type Antrian struct {
	Id     string `json:"id"`
	Status bool   `json:"status"`
	waktu_tiket time.Time `json:"waktu_tiket"`
	cso string `json:"cso"`
}

func AddAntrian() (bool, error) {
	_, _, dataAntrian := GetAntrian()
	var Id string
	var antrianRef *db.Ref
	ref := client.NewRef("antrian")
	var waktu = time.Now()

	if dataAntrian == nil {
		Id = fmt.Sprintf("B-0")
		antrianRef = ref.Child("0")
	} else {
		Id = fmt.Sprintf("B-%d", len(dataAntrian))
		antrianRef = ref.Child(fmt.Sprintf("%d", len(dataAntrian)))
	}
	antrian := Antrian{
		Id:     Id,
		Status: false,
		waktu_tiket :waktu,
	}
	if err := antrianRef.Set(ctx, antrian); err != nil {
		log.Fatal(err)
		return false, err
	}
	return true, nil
}

func GetAntrian() (bool, error, []map[string]interface{}) {
	var data []map[string]interface{}
	ref := client.NewRef("antrian")
	if err := ref.Get(ctx, &data); err != nil {
		log.Fatalln("Error reading from database:", err)
		return false, err, nil
	}

	return true, nil, data
}

func UpdateAntrian(idAntrian string,cso string) (bool, error) {
	ref := client.NewRef("antrian")
	var waktu = time.Now()
	id := strings.Split(idAntrian, "-")
	childRef := ref.Child(id[1])
	antrian := Antrian{
		Id:     idAntrian,
		Status: true,
		waktu_tiket: waktu,
		cso:cso,
	}
	if err := childRef.Set(ctx, antrian); err != nil {
		log.Fatal(err)
		return false, err
	}

	return true, nil
}

func DeleteAntrian(idAntrian string) (bool, error) {

	ref := client.NewRef("antrian")
	id := strings.Split(idAntrian, "-")
	childRef := ref.Child(id[1])
	if err := childRef.Delete(ctx); err != nil {
		log.Fatal(err)
		return false, err
	}

	return true, nil
}

